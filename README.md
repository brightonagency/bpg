# Brighton Agency PDFX Gem (BPG)
## Usage
Creating a Blank PDF/x object(height, width, and bleed are in pts).
```Ruby
    page_box=BrightonPdfx::PageBox.new(width,height,bleed)
    pdfx=BrightonPdfx::Document.generate(title,metadata)
```
Metadata should be structured as an array:
```
["file.icc file location",
"were icc file came from",
"Output Condition Identifier",
"info about file",
"Output Conditions"]
```
Creating a new page in PDFX(``` options``` should be used the same asPrawn::Document.start_new_page(options={})).
```Ruby
    pdfx.start_new_page(page_box, options={})
```

After creating a page use ```pdfx``` or ```pdfx.pdf``` like a normal Prawn::Document using CMYK for colors
```Ruby
    pdfx.pdf.text "Hello World!" # or pdfx.text "Hello World"
```
Will add "Hello World to the pdf at the cursors current position"'

Full Example making a PDF/X-03 with gem
```Ruby
  require "prawn"
  require "bpg"

  metadata=["lib/test.icc",
    "Test file ignore",
    "test",
    "test ICC (ISO 0000-0:0000)",
    "A test ICC"]
  pbox=BrightonPdfx::PageBox.new(300,400,5)
  pdfx=BrightonPdfx::Document.generate("Hello_world",metadata)
  pdfx.start_new_page(pbox)
  pdfx.pdf.text "Hello Word" # or pdfx.text "Hello Word"
  pdfx.pdf.render_file("test.pdf") # or pdfx.text "pdfx.render_file"
```
## Ideas/To-do's
- [x] Clean up metadata and ICCProfileInfo
- [x] Metadata can be passed and made at Document.generate
- [x] PDFX Compliance
- [x] Unit Testing
- [ ] Possible functional methods to wrapper for repeated stuff
- [x] easier font adding
- [x] Improve add_font_directory by making it wait to add fonts till after all folders are checked
- [x] InDesign to Prawn cords method
- [x] Does cordinate converstion behind the scenes
- [x] Allow BrightonPdfx::Document to use pdf functions for ```@pdf```
- [ ] Come up with more stuff

## Goal
Postcard Production Usage

### Bonus
run in bpg directory with terminal to delete all .DS_Store files :)
```
find . -name '.DS_Store' -type f -delete
```
