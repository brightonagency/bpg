require "bpg"
require 'minitest'
require "minitest/autorun"
class TestBPG < MiniTest::Test


  def test_PageBox
    pbox=BrightonPdfx::PageBox.new(300,400,-5)
    assert_equal(pbox.width,300)
    assert_equal(pbox.height,400)
    assert_equal(pbox.bleed,5)
    assert_equal(pbox.bleed_box,[(2*5)+300,(2*5)+400])
    assert_equal(pbox.trim_box,[5+300,5+400])

  end

  #BrightonPdfx::Document Class tests
  def test_Docment_gen
    pdfx=BrightonPdfx::Document.generate("test")
    assert_equal('test', pdfx.title)
    #Need To verify Keys are acceptable.
    #Metadata check as well

  end

  def test_new_page



    # Set up pdfx
    pbox1= BrightonPdfx::PageBox.new(300,400,5)
    pdfx=BrightonPdfx::Document.generate("test")

    #Check if page is of correct size and exist
    pdfx.start_new_page(pbox1)
    assert_equal(1,pdfx.pdf.page_count)
    assert_equal(pbox1.bleed_box, pdfx.pdf.page.size)
    assert_equal(pbox1.trim_box,pdfx.pdf.page.dictionary.data[:TrimBox])
    assert_equal(pbox1.bleed_box,pdfx.pdf.page.dictionary.data[:BleedBox])
    #Checks if adding a 2nd page works of different dimensions
    pbox2=BrightonPdfx::PageBox.new(200,700,3)
    pdfx.start_new_page(pbox2)
    assert_equal(2,pdfx.pdf.page_number)
    assert_equal(2,pdfx.pdf.page_count)
    assert_equal(pbox2.bleed_box,pdfx.pdf.page.size)
    assert_equal(pbox2.trim_box,pdfx.pdf.page.dictionary.data[:TrimBox])
    assert_equal(pbox2.bleed_box,pdfx.pdf.page.dictionary.data[:BleedBox])
    #First page still Okay
    pdfx.pdf.go_to_page(1)
    assert_equal(1,pdfx.pdf.page_number)
    assert_equal(pbox1.bleed_box,pdfx.pdf.page.size)
    assert_equal(pbox1.trim_box,pdfx.pdf.page.dictionary.data[:TrimBox])
    assert_equal(pbox1.bleed_box,pdfx.pdf.page.dictionary.data[:BleedBox])
  end

  def test_to_Prawn
    # Set up pdfx
    pbox= BrightonPdfx::PageBox.new(300,400,5)
    pdfx=BrightonPdfx::Document.generate("test")
    pdfx.start_new_page(pbox,margin:0)

    assert_equal([6,395], pdfx.to_Prawn(1,10))
    assert_equal([6,395],pdfx.to_Prawn([1,10]))

  end

  def test_set_background

    # Set up pdfx
    pbox= BrightonPdfx::PageBox.new(300,400,5)
    pdfx=BrightonPdfx::Document.generate("test")
    pdfx.start_new_page(pbox,margin:0)

    assert_equal(410,pdfx.set_background(File.absolute_path('test/test.jpg')).scaled_height)
    assert_equal(310,pdfx.set_background(File.absolute_path('test/test.jpg')).scaled_width)
    pbox= BrightonPdfx::PageBox.new(300,200,5)
    pdfx.start_new_page(pbox,margin:0)
    assert_equal(210,pdfx.set_background(File.absolute_path('test/test.jpg')).scaled_height)
    assert_equal(310,pdfx.set_background(File.absolute_path('test/test.jpg')).scaled_width)
  end

  def test_add_font
    pdfx=BrightonPdfx::Document.generate("test")
    expected_families = {"Courier"=>{:bold=>"Courier-Bold", :italic=>"Courier-Oblique", :bold_italic=>"Courier-BoldOblique", :normal=>"Courier"}, "Times-Roman"=>{:bold=>"Times-Bold", :italic=>"Times-Italic", :bold_italic=>"Times-BoldItalic", :normal=>"Times-Roman"}, "Helvetica"=>{:bold=>"Helvetica-Bold", :italic=>"Helvetica-Oblique", :bold_italic=>"Helvetica-BoldOblique", :normal=>"Helvetica"}, "Arial"=>{:bold=>"/Users/davidt/Desktop/bpg/fonts/Arial-Bold.ttf", :normal=>"not given"}}
    #test add_font
    pdfx.add_font("Arial", {bold: File.absolute_path('fonts/Arial-Bold.ttf')})
    assert_equal(expected_families,pdfx.pdf.font_families)

    #test add folder
    pdfx.add_font_folder(File.absolute_path('test/fonts/deltapine'))
    expected_families = {"Courier"=>{:bold=>"Courier-Bold", :italic=>"Courier-Oblique", :bold_italic=>"Courier-BoldOblique", :normal=>"Courier"}, "Times-Roman"=>{:bold=>"Times-Bold", :italic=>"Times-Italic", :bold_italic=>"Times-BoldItalic", :normal=>"Times-Roman"}, "Helvetica"=>{:bold=>"Helvetica-Bold", :italic=>"Helvetica-Oblique", :bold_italic=>"Helvetica-BoldOblique", :normal=>"Helvetica"}, "Arial"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Arial-Bold.ttf", :normal=>"not given"}, "Aachen"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Aachen-Bold.ttf", :normal=>"not given"}, "AachenBT"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/AachenBT.ttf"}, "AvenirNextCondensed"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/AvenirNextCondensed-Regular.ttf"}, "HelveticaNeueLTStd"=>{:BdCn=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/HelveticaNeueLTStd-BdCn.ttf", :Cn=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/HelveticaNeueLTStd-Cn.ttf", :LtCn=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/HelveticaNeueLTStd-LtCn.ttf", :normal=>"not given"}, "Interstate"=>{:Black=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-Black.ttf", :BlackCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-BlackCondensed.ttf", :BoldCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-BoldCondensed.ttf", :BoldCondItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-BoldCondItalic.ttf", :LightCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-LightCondensed.ttf", :LightCondItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-LightCondItalic.ttf", :LightItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-LightItalic.ttf", :RegularCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-RegularCondensed.ttf", :RegularCondItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-RegularCondItalic.ttf", :normal=>"not given"}}
    assert_equal(expected_families,pdfx.pdf.font_families)

    # Test directory add font
    pdfx.add_font_directory(File.absolute_path('test/fonts'))
    expected_families = {"Courier"=>{:bold=>"Courier-Bold", :italic=>"Courier-Oblique", :bold_italic=>"Courier-BoldOblique", :normal=>"Courier"}, "Times-Roman"=>{:bold=>"Times-Bold", :italic=>"Times-Italic", :bold_italic=>"Times-BoldItalic", :normal=>"Times-Roman"}, "Helvetica"=>{:bold=>"Helvetica-Bold", :italic=>"Helvetica-Oblique", :bold_italic=>"Helvetica-BoldOblique", :normal=>"Helvetica"}, "Arial"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Arial-Bold.ttf", :normal=>"not given"}, "Aachen"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Aachen-Bold.ttf", :normal=>"not given"}, "AachenBT"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/AachenBT.ttf"}, "AvenirNextCondensed"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNextCondensed-Bold.ttf", :DemiBold=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNextCondensed-DemiBold.ttf", :Italic=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNextCondensed-Italic.ttf", :normal=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/AvenirNextCondensed-Regular.ttf"}, "HelveticaNeueLTStd"=>{:BdCn=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/HelveticaNeueLTStd-BdCn.ttf", :BdCnO=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/HelveticaNeueLTStd-BdCnO.ttf", :LtCn=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/HelveticaNeueLTStd-LtCn.ttf", :Cn=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/HelveticaNeueLTStd-Cn.ttf", :normal=>"not given"}, "Interstate"=>{:Black=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/Interstate-Black.ttf", :BlackCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-BlackCondensed.ttf", :BoldCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/Interstate-BoldCondensed.ttf", :BoldCondItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-BoldCondItalic.ttf", :LightCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/Interstate-LightCondensed.ttf", :LightCondItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-LightCondItalic.ttf", :LightItalic=>"/Users/davidt/Desktop/bpg/test/fonts/Interstate-LightItalic.ttf", :RegularCondensed=>"/Users/davidt/Desktop/bpg/test/fonts/regional_brands/Interstate-RegularCondensed.ttf", :RegularCondItalic=>"/Users/davidt/Desktop/bpg/test/fonts/deltapine/Interstate-RegularCondItalic.ttf", :normal=>"not given"}, "MinionPro"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/MinionPro-Regular.ttf"}, "MorganSansCond"=>{:BoldItalicLining=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/MorganSansCond-BoldItalicLining.ttf", :BoldLining=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/MorganSansCond-BoldLining.ttf", :ItalicLining=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/MorganSansCond-ItalicLining.ttf", :RegularLining=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/MorganSansCond-RegularLining.ttf", :normal=>"not given"}, "TradeGothic"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/TradeGothic-Bold.ttf", :CondEighteen=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/TradeGothic-CondEighteen.ttf", :normal=>"not given"}, "TradeGothicLTCondensedNo18.alt"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/asgrow_soy/TradeGothicLTCondensedNo18.alt.ttf"}, "TradeGothicLTStd"=>{:BdCn20=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/TradeGothicLTStd-BdCn20.ttf", :Bold=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/TradeGothicLTStd-Bold.ttf", :Cn18=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/TradeGothicLTStd-Cn18.ttf", :normal=>"not given"}, "AvenirNext"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNext-Bold.ttf", :DemiBold=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNext-DemiBold.ttf", :normal=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNext-Regular.ttf"}, "AvenirNextDemi"=>{:Bold=>"/Users/davidt/Desktop/bpg/test/fonts/AvenirNextDemi-Bold.ttf", :normal=>"not given"}, "BrandBlend"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/BrandBlend.ttf"}, "ChaletNewYorkNineteenSixty"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/ChaletNewYorkNineteenSixty-Regular.ttf"}, "CustomSymbols.2"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/CustomSymbols.2.ttf"}, "CustomSymbols.exp"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/CustomSymbols.exp.ttf"}, "CustomSymbols"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/dekalb_corn/CustomSymbols.ttf"}, "HelveticaNeueLtCom47Condensed"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/HelveticaNeueLtCom47Condensed-Regular.ttf"}, "HelveticaNeueLtCom47LightCondensed"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/HelveticaNeueLtCom47LightCondensed-Regular.ttf"}, "InterstateLightCondensed"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/InterstateLightCondensed-Regular.ttf"}, "ZapfDingbats"=>{:normal=>"/Users/davidt/Desktop/bpg/test/fonts/ZapfDingbats-Regular.ttf"}}
    assert_equal(expected_families,pdfx.pdf.font_families)
    # Test directory add font
  end


  def test_method_missing
    pdfx=BrightonPdfx::Document.generate("test")
    pbox= BrightonPdfx::PageBox.new(300,400,5)
    pdfx.start_new_page(pbox,margin:0)
    assert_raises NoMethodError do
      (pdfx.Hello)
    end
    assert_raises ArgumentError do
      pdfx.text 'hello', at:[21,12]
    end
    assert(pdfx.text_box('hello'), at: [12,21])

  end

end
