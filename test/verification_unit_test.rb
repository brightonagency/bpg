require 'verification'
require 'minitest'
require "minitest/autorun"
require "pdf/reader"
class TestVerification < MiniTest::Test


  DIRECTORY = File.absolute_path('test/pdfs')+"/"


  def test_metadata
    verify= Verificattion::Pdfx_verificaion.new()
    assert_equal(true,verify.verify_metadata(File.absolute_path("test/pdfs/valid.pdf")))
    assert_equal(false,verify.verify_metadata(File.absolute_path("test/pdfs/Invalid.pdf")))
    assert_equal(false,verify.verify_metadata(File.absolute_path("test/pdfs/invalidfonts.pdf")))
  end


  def test_bleed
    verify= Verificattion::Pdfx_verificaion.new()
    assert_equal(true,verify.verify_bleed(File.absolute_path("test/pdfs/valid.pdf")))
    assert_equal(true,verify.verify_bleed(File.absolute_path("test/pdfs/Invalid.pdf")))
    assert_equal(false,verify.verify_bleed(File.absolute_path("test/pdfs/badbleed.pdf")))

  end


  def test_fonts
    verify= Verificattion::Pdfx_verificaion.new()
    assert_equal(true,verify.verify_fonts(File.absolute_path("test/pdfs/valid.pdf")))
    assert_equal(false,verify.verify_fonts(File.absolute_path("test/pdfs/Invalid.pdf")))
    assert_equal(false,verify.verify_fonts(File.absolute_path("test/pdfs/invalidfonts.pdf")))
  end


  def test_line_check
    verify= Verificattion::Pdfx_verificaion.new()
    assert_equal(true,verify.linecheck(File.absolute_path("test/pdfs/valid.pdf")))
    assert_equal(false,verify.linecheck(File.absolute_path("test/pdfs/Invalid.pdf")))
    assert_equal(true,verify.linecheck(File.absolute_path("test/pdfs/invalidfonts.pdf")))
  end

  def test_verify
    verify= Verificattion::Pdfx_verificaion.new()
    assert_equal(true,verify.verify(File.absolute_path("test/pdfs/valid.pdf")))
    assert_equal(false,verify.verify(File.absolute_path("test/pdfs/Invalid.pdf")))
    assert_equal(false,verify.verify(File.absolute_path("test/pdfs/invalidfonts.pdf")))

  end

end
