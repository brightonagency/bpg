# BPD UNIT TESTING INFO

## Functions
### PageBox
* Initialize
  * Accessors work
  * Value are correct

### Icc Profile
* Accessors work
* Initialize(args[])
  * Returns :Profile as bytes
  * Values set correctly

### Document
* generate()
  * returns new Document
  * @pdf no pages, info=pdfx(keys)
  * Add Metadata
    * pdf.state.trailer=trailer_keys
    * add_output_intents!
      * profile
      * intent
      * pdf.state.store.root.data=intent

# Tested by Unit Test
### PageBox
- [x] Initialize
  - [x] Accessors work
  - [x] Value are correct

### Icc Profile
 - [ ] Accessors work
 - [ ] Initialize(args[])
   - [ ] Returns :Profile as bytes
   - [ ] Values set correctly

### Document
 - [x] generate()
   - [x] returns new Document
   - [ ] @pdf no pages, info=pdfx(keys)
   - [ ] Add Metadata
     - [ ] pdf.state.trailer=trailer_keys
     - [ ] add_output_intents!
       - [ ] profile
       - [ ] intent
       - [ ] pdf.state.store.root.data=intent


# Running the Test
From bpg directory in terminal

    ruby -I lib test/unit_test.rb
