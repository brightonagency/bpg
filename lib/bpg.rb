#Required
require 'prawn'
require 'securerandom'
require 'mini_magick'
require "prawn/table"

# Create a PageBox with bleed_box trim_box in pts
# Example of use
# pbox = BrightonPdfx::PageBox.new(500, 720, 3)
# pdf=BrightonPdfx::Document.generate()
# pdf.start_new_page(pbox, margin: 0)
# pdf.pdf.text "testing"
# pdf.pdf.render_file("test.pdf")
# Creates a pdf/x of 500 width 720 height 3 bleed
# with testing under the name test.pdf

#Make PDF/X document
module BrightonPdfx
  class ICCProfileInfo
    attr_reader :registry_name, :output_condition_identifier, :info, :output_condition, :profile_bytes

    def initialize(attrs = {})
      [:registry_name, :output_condition_identifier, :info, :output_condition].each do |attr_name|
          instance_variable_set(:"@#{attr_name}", attrs.fetch(attr_name))
        end

      @profile_bytes = Pathname.new(attrs.fetch(:profile)).binread
    end
  end

  # IIRC, I got these values by examining the metadata in an InDesign
  # generated PDF/X document
  #
  # You can download the ICC files themselves from Adobe at
  # http://www.adobe.com/support/downloads/iccprofiles/iccprofiles_win.html
  FOGRA39 = ICCProfileInfo.new({
    registry_name: "http://www.color.org",
    output_condition_identifier: "FOGRA39",
    info: "Coated FOGRA39 (ISO 12647-2:2004)",
    output_condition: "Offset commercial and specialty printing according to ISO 12647-2:2004 / Amd 1, paper type 1 or 2 (gloss or matte coated offset, 115 g/m2), screen frequency 60/cm.",
      profile: File.absolute_path('ruby/bpg/lib/CoatedFOGRA39.icc')
  })

  #Defines boxes and values for boxes
  class PageBox
    #Initialize demensions of PDF/X File
    attr_reader :width, :height, :bleed, :bleed_box, :trim_box

    def initialize(width, height, bleed)
      @width = width
      @height = height
      @bleed = bleed
      @bleed_box = [(2 * bleed) + width,(2 * bleed) + height]
      @trim_box = [bleed + width,bleed + height]
    end
  end


  class Document
    #Required to make PDFX version
    PDFX_VERSION = "PDF/X-1a:2003"
    attr_reader :pdf, :title
    attr_accessor :fastdev

    # Creation of Document returns BrightonPdfx::Document.new with a class
    # variable @pdf which is of type Prawn::Document
    def self.generate(title)
      @fastdev = false
      new(title)
    end

    def initialize(title)
      @title = title
      @pdf = Prawn::Document.new(skip_page_creation: true, info: info_keys)
      add_pdfx_metadata!
    end


    #Creates new page with size of Page_box and hash of options
    def start_new_page(page_box, options={})
      pdf.start_new_page(options.merge(size: page_box.bleed_box))
      pdf.page.dictionary.data[:BleedBox] = page_box.bleed_box
      pdf.page.dictionary.data[:TrimBox] = page_box.trim_box
    end


    # ----Wrapper/Functional Functions---------------------------------------------------------------
    #Convert InDesign x,y to Prawn [x,y] value for prawn cord accepts x,y or [x,y] as inputs
    def to_Prawn(*args)
      bleed=(pdf.page.dictionary.data[:BleedBox][1]-pdf.page.dictionary.data[:TrimBox][1])
      cord_a = Array(args)
      cord_a = args.first if args.first.kind_of? Array
      return [cord_a[0].to_i+bleed,(@pdf.page.size[1].to_i-bleed)-cord_a[1].to_i]
    end

    #Returns smaller size if current size is to big to fit
    def shrink_to_fit(text,font, size, width, height)
      #we will have one for testing
      actual_width = pdf.width_of text, font: font , size: size, margin: 5, left_margin: 0, right_margin: 0
      actual_height = pdf.height_of text, size: size, margin:0, left_margin: 0, right_margin: 0

      if (actual_width > width) || (actual_height > height)
        puts actual_width
        width_size = size * width / (actual_width*1.0)
        height_size = size * height / (actual_height*1.0)
        # return smaller of the 2
        puts pdf.width_of text, font: font , size: width_size, margin: 0, left_margin: 0, right_margin: 0
        return width_size > height_size ? height_size : width_size
      end
      return size
    end


    #Given a file to use for a back ground sets the file to the height and width of the current page
    def set_background(file)
        image file.to_s, width: @pdf.page.size[0], height: @pdf.page.size[1]
    end

    # Add font("family name", types) types is hash of of :key=>'font file location'
    def add_font(name,file)
      @pdf.font_families.update("#{name}"=> {:normal => "#{file}"})
    end

    # Adds all fonts given directory (fonts saved as full name AvenirNext-Bold.tff will be saved as a 'AvenirNext-Bold' )
    def add_font_directory(directory)
      Dir.children(directory).each do |file|
        if File.extname(file)==".ttf"
          name = parse_font(file)
          add_font(name, "#{directory}/" + "#{file}")
        elsif File.directory?("#{directory}/"+file)
            add_font_directory("#{directory}/"+file)
        end
      end
    end


    #ICC profile and metadata stuff
    private
      def make_jpg(filename,basename)
        convert = MiniMagick::Image.open(filename)
        convert.format "jpg"
        convert.write basename+".jpg"
      end
      #used to parst file into [family, style]
      def parse_font(filename)
        name_style=File.basename(filename,".ttf")
        return name_style
      end

      # METADATA STUFF -----------------------------------------------------------
      def add_pdfx_metadata!
        pdf.state.trailer = trailer_keys
        add_output_intents!
      end

      def add_output_intents!
        # The data here declares this a CMYK PDF/X ICC profile- 4 colours,
        # with each having a value range of 0-1
        # Then we add and compress the bytes of the ICC profile itself
        profile = pdf.ref!({N: 4, Range: [0, 1, 0, 1, 0, 1, 0, 1]})
        profile << FOGRA39.profile_bytes
        profile.stream.compress!
        intent = [{
          Type: :OutputIntent,
          S: :GTS_PDFX,
          RegistryName: PDF::Core::LiteralString.new(FOGRA39.registry_name),
          OutputConditionIdentifier: PDF::Core::LiteralString.new(FOGRA39.output_condition_identifier),
          Info: PDF::Core::LiteralString.new(FOGRA39.info),
          OutputCondition: PDF::Core::LiteralString.new(FOGRA39.output_condition),
          DestOutputProfile: profile
        }]
        pdf.state.store.root.data[:OutputIntents] = intent
      end

      #Verify this is what we need and remove parts that have no value
      def trailer_keys
        id=SecureRandom.uuid
        {:ID => [id,id]}
      end

      def pdfx_keys(info_dict)
        info_dict.merge({:GTS_PDFXVersion=>PDFX_VERSION, :GTS_PDFXConformance=>PDFX_VERSION})
      end

      def trapped_key(info_dict)
        info_dict.merge({:Trapped => :False})
      end

      def title_key(info_dict)
        info_dict.merge({:Title => title})
      end

      def date_keys(info_dict)
        time = Time.now
        info_dict.merge({ModDate: time, CreationDate: time})
      end

      def info_keys
        date_keys(title_key(pdfx_keys(trapped_key({}))))
      end

      #Overloaded to allow easier use of pdf functions from BrightonPdfx::Document object
      def method_missing(name,*args)
          #checks fast dev mode anytime image is used
        if name.to_s == 'image' && @fastdev
          basename = *args[0].split('.')[0]
          if !File.file?(basename[0]+'.jpg')
            make_jpg(*args[0],basename[0])
          end
          @pdf.send(name,basename[0]+'.jpg', *args[1..-1])
        else
          @pdf.send(name,*args)
        end
      end
  end
end
