# PDF VERIFICATOIN

require 'pdf/reader'


module ExtractFonts

  class Extractor
    def page(page)
      count = 0

      return count if page.fonts.nil? || page.fonts.empty?

      page.fonts.each do |label, font|
        complete_refs[font] = true if complete_refs[font]

        process_font(page, font)

        count += 1
      end

      count
    end

    private

    def process_font(page, font)
      font = page.objects.deref(font)

      case font[:Subtype]
      when :Type0 then
        font[:DescendantFonts].each { |f| process_font(page, f) }
      when :TrueType, :CIDFontType2 then
        ExtractFonts::TTF.new(page.objects, font).save("#{font[:BaseFont]}.ttf")
      else
        $stderr.puts "unsupported font type #{font[:Subtype]} for #{font[:BaseFont]}"
        puts font
      end
    end

    def complete_refs
      @complete_refs ||= {}
    end

  end

  class TTF

    def initialize(objects, font)
      @extracted=[]
      @failed=[]
      @objects, @font = objects, font
      @descriptor = @objects.deref(@font[:FontDescriptor])
    end

    def save(filename)
      if @descriptor && @descriptor[:FontFile2]
        @extracted.push(filename)
      else
        puts "- TTF font not embedded"
        @failed.push(filename)
      end
    end
  end
end
