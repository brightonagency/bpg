Gem::Specification.new do |s|
  s.name        = 'bpg'
  s.version     = '0.0.1'
  s.date        = '2018-10-26'
  s.summary     = "Creates pdfx that can be edited"
  s.description = "A Gem to make pdfx classes"
  s.authors     = ["David Tutt"]
  s.email       = 'David.Tutt@brightonagency.com'
  s.files       = ["lib/bpg.rb"]
  s.homepage    = ''
  s.license     = ''
end
