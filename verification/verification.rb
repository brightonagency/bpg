
module Verificattion
    require "pdf/reader"
  class Pdfx_verificaion
    def initialzie
      self
    end

    def verify(filename)
      verified=false
      verified = true if linecheck(filename) && verify_fonts(filename) && verify_bleed(filename) && verify_metadata(filename)
      return verified
    end
      # Verify OutputIntent Exist and for embbeded urls
    def linecheck(filename)
      flag=true
      flag2=true
      printNextline = false
      File.open(filename, "r") do |f|
        f.each_line do |line|
          if line=="/Outputintents"
            flag=false
          end
          if line == "/S /URI"
            puts "Embbeded URL"
            flag2=false
            printNextline = true
          end
          if printNextline
            printNextline = false
            puts line
          end
          # Need to deal with utf-8 to accept
          begin

            if line.include?"JavaScript"
              puts "JavaScript found in document"
              flag2 = false
            end
          end
        end
      end
      puts "No Output Intent x Found" if !flag
      flag && flag2


    rescue ArgumentError
        flag
    end


    # Embedded fonts exist
    def verify_fonts(filename)
      flag=true
      PDF::Reader.open(filename) do |reader|
        reader.pages.each do |page|
          page.fonts.each do |font|
            if !font[1][:FontDescriptor]
              puts "#{font[1][:BaseFont]}" + " no Font Descriptor"
              flag=false
            elsif !font[1][:FontDescriptor][:FontFile2]
              puts "#{font[1][:BaseFont]}" + " no font file"
              flag=false
            end
          end
        end
      end

     return flag
    end

    # Bleed less or equal to  Trim
    def verify_bleed(filename)
      flag=true
      File.open(filename, "rb") do |io|

        reader = PDF::Reader.new(io)
        count = reader.page_count
        while count > 0
          bleed = reader.page(count).attributes[:BleedBox]
          trim = reader.page(count).attributes[:TrimBox]
          count = count - 1
          next if !bleed
          width = trim.size - 2
          height = trim.size - 1
          if (bleed[width] - trim[width]) < 0
            puts "Bleed Box width is less than Trim Box width"
            flag=false
          end
          if (bleed[height] - trim[height]) < 0
              puts "Bleed Box height is less than Trim Box height"
              flag=false
          end
          if reader.page(1).attributes[:Artbox] && reader.page(1).attributes[:TrimBox]
            puts "Can't have both Artbox and a TrimBox"
            flag=false
          end
        end
      end

      return flag
    end

    # PDF Meta has trapped or not
    # PDF Meta claims to be PDF/X-1A
    # Verify PDF version is 1.4
    def verify_metadata(filename)
      flag = true
      File.open(filename, "rb") do |io|
        reader = PDF::Reader.new(io)
        info= reader.info

        if (info[:Trapped]).nil?
          puts "Trapped not defined"
          flag=false
        end

        if info[:GTS_PDFXVersion] != "PDF/X-1a:2003"
          puts "PDFX version not defined as PDF/X-1a:2003"
          flag=false
        end

        if info[:GTS_PDFXConformance]!="PDF/X-1a:2003"
          puts "PDFX Confromance not declared as PDF/X-1a:2003"
          flag=false
        end
      end

      reader = PDF::Reader.new(filename)
      if "#{reader.pdf_version}" != "1.3"
        puts "PDF wrong version needs to be 1.3 PDF version is #{reader.pdf_version}"
        flag=false
      end

      return flag
    end


  end
end
